// The contents of this file are in the public domain. See LICENSE_FOR_EXAMPLE_PROGRAMS.txt
/*

    This example program shows how to find frontal human faces in an image.  In
    particular, this program shows how you can take a list of images from the
    command line and display each on the screen with red boxes overlaid on each
    human face.

    The examples/faces folder contains some jpg images of people.  You can run
    this program on them and see the detections by executing the following command:
        ./face_detection_ex faces/*.jpg

    
    This face detector is made using the now classic Histogram of Oriented
    Gradients (HOG) feature combined with a linear classifier, an image pyramid,
    and sliding window detection scheme.  This type of object detector is fairly
    general and capable of detecting many types of semi-rigid objects in
    addition to human faces.  Therefore, if you are interested in making your
    own object detectors then read the fhog_object_detector_ex.cpp example
    program.  It shows how to use the machine learning tools which were used to
    create dlib's face detector. 


    Finally, note that the face detector is fastest when compiled with at least
    SSE2 instructions enabled.  So if you are using a PC with an Intel or AMD
    chip then you should enable at least SSE2 instructions.  If you are using
    cmake to compile this program you can enable them by using one of the
    following commands when you create the build project:
        cmake path_to_dlib_root/examples -DUSE_SSE2_INSTRUCTIONS=ON
        cmake path_to_dlib_root/examples -DUSE_SSE4_INSTRUCTIONS=ON
        cmake path_to_dlib_root/examples -DUSE_AVX_INSTRUCTIONS=ON
    This will set the appropriate compiler options for GCC, clang, Visual
    Studio, or the Intel compiler.  If you are using another compiler then you
    need to consult your compiler's manual to determine how to enable these
    instructions.  Note that AVX is the fastest but requires a CPU from at least
    2011.  SSE4 is the next fastest and is supported by most current machines.  
*/

#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <iostream>
#include <fstream>
#include <sys/time.h>
#include "../version.h"
#include "../dlib/image_processing/box_overlap_testing.h"
#include "../dlib/image_transforms/image_pyramid.h"
#include <omp.h>
#include <math.h>

using namespace dlib;
using namespace std;

extern void initialize_profiler();
extern void end_profiler();
extern void gpuDetectFaces(unsigned char* image, const int no_scales);
extern void gpuGetFaces(float* host_faces, const int no_scales);      
extern void gpu_allocate_memory_constant_image_size(int height_org_img, int width_org_img, int no_scales, int cell_size, int scale_factor, 
    int padding_rows_offset, int padding_cols_offset, int filter_rows_padding, int filter_cols_padding);

// ----------------------------------------------------------------------------------------

// Function to see how much memory should be allocated on the GPU.
// Based on the height and width of the first image of the sequence
// we compute how many times the image needs to be downscaled in a 
// pyramid-like fashion in order to detect faces from all sizes. 
// Based on this number of scales, a certain amount of memory is 
// allocated on the GPU for all scales of the images for feature 
// extraction and classification steps

void initialize_gpu_memory(frontal_face_detector detector, int& no_scales, dlib::pyramid_down<6>& pyr, string initial_image_name, int& width_org_img, int& height_org_img)
{
    array2d<unsigned char> initial_image;
    load_image(initial_image, initial_image_name);


    // "pyramid_up" can be used to upscale the image to detect smaller images. 
    // However, in the current eyeblink-conditioning setup this is not necessary
    // The biggest scale is even skipped because it searches for relatively small
    // faces in the image, out of the requirements for this project (>20% of the image)
    // NOTE THAT IF YOU UNCOMMENT IT HERE, YOU ALSO HAVE TO UNCOMMENT IT IN THE 
    // combineThreadImages FUNCTION!

    // pyramid_up(initial_image);

    width_org_img = initial_image.nc();
    height_org_img = initial_image.nr();

    // This refers to to the original Dlib code to and uses its characteristics in order
    // to get the variables needed to compute how many times the original image should be 
    // downscaled

    const unsigned long min_pyramid_layer_width = detector.get_scanner().get_min_pyramid_layer_width();
    const unsigned long min_pyramid_layer_height = detector.get_scanner().get_min_pyramid_layer_height();
    const unsigned long max_pyramid_levels = detector.get_scanner().get_max_pyramid_levels();
    const unsigned long cell_size = detector.get_scanner().get_cell_size();
    const unsigned long width = detector.get_scanner().get_fhog_window_width();
    const unsigned long height = detector.get_scanner().get_fhog_window_height();
    const int padding_rows_offset = (height-1)/2;
    const int padding_cols_offset = (width-1)/2;
    no_scales = 0;  

    rectangle rect_img;
    rect_img = resize_rect(rect_img, width_org_img, height_org_img);

    // The amount of times the image needs to be downscaled in this loop. The image is downscaled
    // until either the width or height of the image is smaller than those of the detection window (80 x 80) 

    double scale_factor = pyramid_rate(pyr); //0.83333
    int N = int((scale_factor)/(1-scale_factor)+1); //6 
    do
    {
        rect_img = pyr.rect_down(rect_img);
        ++no_scales;
    } while (rect_img.width() >= min_pyramid_layer_width && rect_img.height() >= min_pyramid_layer_height &&
        no_scales < max_pyramid_levels);


    // Based on the number of scales and the width and height of the image, memory is allocated on the gpu
    // for all image sizes, intermediate feature and classification steps (see "cuda_face_detection_gpu.cu") file

    gpu_allocate_memory_constant_image_size(height_org_img, width_org_img, no_scales, cell_size, N, padding_rows_offset, padding_cols_offset, height, width);
}

// Helper function for non-maximum suppression to see if any of the detections 
// are of the same object. 

bool overlaps_any_box(const std::vector<rectangle>& rects, const dlib::rectangle& rect, test_box_overlap boxes_overlap)
{
    for (unsigned long i = 0; i < rects.size(); ++i)
    {
        if (boxes_overlap(rects[i], rect))
            return true;
    }
    return false;   
}

// Each CPU thread receives its own set of faces in the host_faces array. The final element of the 
// array tells how many faces there were detected in total. The results are stored in the array 
// coming for the GPU as folows: 
//  x = x-coordinate of detection
//  y = y-coordinate of detection
//  score = detection score of face
//  scale = scale on which face was detected
//  fb = detector number that detected the face (one of the five rotations)
// This array is converted to a number of rect objects that are used in the Dlib library later for 
// the landmark estimation. The detections are scaled to the original image size, based on the scale
// that they are detected.

void convert_to_rects(float* host_faces, std::vector<double>& threshs, std::vector<rect_detection> dets_accum, 
    std::vector<rectangle>& dets, frontal_face_detector detector, dlib::pyramid_down<6>& pyr)
{
    // Last array element that tells how many detections were found
    int no_faces = (int)host_faces[MAX_NO_SCALES*NO_FILTERS*MAX_FACES*5];

    // Detections are ocnverted to rects and scaled up to correspond to original image size
    for(int i=0;i<no_faces;++i)
    {
        int x = int(host_faces[5*i]);
        int y = int(host_faces[5*i+1]);
        float score = host_faces[5*i+2];
        int scale = int(host_faces[5*i+3]);
        int fb = int(host_faces[5*i+4]);
        rectangle rect = detector.get_scanner().get_feature_extractor().feats_to_image(dlib::centered_rect(dlib::point(x,y),
          detector.get_scanner().get_fhog_window_width()-2*detector.get_scanner().get_padding(), detector.get_scanner().get_fhog_window_height()-2*detector.get_scanner().get_padding()),
        detector.get_scanner().get_cell_size(), detector.get_scanner().get_fhog_window_height(), detector.get_scanner().get_fhog_window_width());
        rect = pyr.rect_up(rect, scale); // CHECKEN DOET SCALE T M?
        rect_detection temp;
        temp.detection_confidence = score-threshs[fb];
        temp.weight_index = fb;
        temp.rect = rect; 
        dets_accum.push_back(temp);   
    }

    // Non-maximum suppression step. Detections that come from the same face (overlap more than 50%) 
    // are brought back to one detection. The detection with the highest score remains.

    dets.clear();
    std::sort(dets_accum.rbegin(), dets_accum.rend());
    for (unsigned long i = 0; i < dets_accum.size(); ++i)
    {
        if (overlaps_any_box(dets, dets_accum[i].rect, detector.get_overlap_tester()))
            continue;

        dets.push_back(dets_accum[i].rect);
    }

    // In the current model we assume that only one face is detect in each image. In the case that there
    // are more faces detected, the vector of faces is sorted based on detection size. The face with the 
    // largest area is put in from of the vector (index 0) and the landmark detection is only performed
    // on that face (see detectLandmark function)

    std::sort(dets.begin(), dets.end(),[](rectangle a, rectangle b) {
        return b.area() < a.area();   
    });
    dets_accum.clear();
}

// This image is executed once for each CPU thread because it is called from within an OMP parallel region
// Each thread loads an image from memory into an array_2d<unsigned char> object. This includes decoding
// the image from whatever file format it is stored in. The images get stored from this object into a 
// one dimensional array of unsigned chars. When each thread has finished this, it copies it to a large shared
// array that contains the images from all threads, which gets send to the GPU 

void combineThreadImages(array2d<unsigned char>& thread_img, unsigned char* image, const std::vector<string>& images_in_dir, const int& no_iterations, const int& i, const int& no_images, const int img_size)
{
    int thread_id = omp_get_thread_num();
    unsigned char* thread_image_array = new unsigned char[img_size];
    
    // The if-else structure here is for when the total number of images is not divisible by the total number
    // of CPU threads (see version.h there you can specify the number of CPU threads). Black images are then 
    // added to the ''tail'' of the image sequence to make it divisible and contain algorithm execution as
    // normal

    if(thread_id*no_iterations+i < no_images)
    {
        load_image(thread_img, images_in_dir[thread_id*no_iterations+i]);
        // pyramid_up(thread_img);

        for(int r = 0; r<thread_img.nr(); r++)
        {
            for(int c = 0; c<thread_img.nc(); c++)
            {
                thread_image_array[r*thread_img.nc()+c] = dlib::get_pixel_intensity(thread_img[r][c]);
            }
        }
        // Here the thread-private image is copied to one larger shared array
        memcpy(image+thread_id*thread_img.nc()*thread_img.nr(),thread_image_array,img_size*sizeof(unsigned char));
    }
    else
    {  
        for(int r = 0; r<thread_img.nr(); r++)
        {
            for(int c = 0; c<thread_img.nc(); c++)
            {
                thread_image_array[r*thread_img.nc()+c] = 0;
            }
        }
        memcpy(image+thread_id*thread_img.nc()*thread_img.nr(),thread_image_array,img_size*sizeof(unsigned char));
    }
}

// Helper function in case you want to see which CPU thread is doing what

void print_proc_affinity()
{
    printf( "Hello world from thread %d of %d running on cpu %2d!\n", 
        omp_get_thread_num(), 
        omp_get_num_threads(),
        sched_getcpu());
}

// Debug mode which shows which image is being processed, shows the image as well as the face and landmark detections

void debugPrint(const std::vector<string>& images_in_dir, std::vector<dlib::rectangle>& dets, array2d<unsigned char>& previous_img, std::vector<full_object_detection>& shapes, int img_number)
{
    cout << "processing image " << images_in_dir[img_number] << endl;
    cout << "Number of faces detected: " << dets.size() << endl;
    image_window win;

    win.clear_overlay();
    win.set_image(previous_img);
    win.add_overlay(render_face_detections(shapes[0]));   
    win.add_overlay(dets, rgb_pixel(255,0,0)); 
    cout << "Hit enter to process the next image..." << endl;
    cin.get();

}

// Each thread copies a part of the array of face detections that is sent back by the GPU to its own private memory. 
// The part of the array it copies corresponds to its thread id. The detection in the GPU array are converted to 
// dlib rectangles that can be used as input for the landmark detection algorithm. When no faces are detected, 0s are
// written to the vector containing the landmark locations for that frame

void detectLandmarks(float* thread_host_faces, float* host_faces, int thread_id, std::vector<double>& threshs, std::vector<rect_detection>& dets_accum, 
    std::vector<rectangle>& dets, frontal_face_detector& detector, pyramid_down<6>& pyr, shape_predictor& sp, array2d<unsigned char>& previous_img, std::vector<int>* landmarks)
{
    // The GPU array is divided between the CPU threads
    memcpy(thread_host_faces, host_faces+thread_id*(MAX_NO_SCALES*NO_FILTERS*MAX_FACES*5+1), (MAX_NO_SCALES*NO_FILTERS*MAX_FACES*5+1)*sizeof(float));
    // Array is converted to dlib rects
    convert_to_rects(thread_host_faces, threshs, dets_accum, dets, detector, pyr);
    
    // In case faces have been detected, the biggest one is used for landmark detection. We assume in the current setup
    // that only one face will be detected, but the landmark detection could run on multiple faces from the same image
    // by adjusting the code beneath. If no faces are detected, 0s are written to the vector.

    if(!dets.empty())
    {
        // The complete landmark detectoin happens in the following line. It is implemented in the dlib library,
        // and nothing has to be changed there. The image_processing and image_transform folders of the dlib folder
        // contain most of the files that are used for this algorithm if you want to take a closer look

        dlib::full_object_detection shape = sp(previous_img, dets[0]);
        for(int i=0;i<68;++i)
        {
            landmarks[i].push_back(shape.part(i).x());
            landmarks[i].push_back(shape.part(i).y());
        }  
    }
    else
    {
        printf("No faces detected\n");
        for(int i=0;i<68;++i)
        {
            landmarks[i].push_back(0.0f);
            landmarks[i].push_back(0.0f);
        }  
    }
}

// Writing landmarks to file

void writeLandmarks(std::vector<int>* landmarks, string filename, int no_images)
{
    ofstream myfile;
    myfile.open (filename);
    std::cout << "Writing to " << filename << std::endl;
    for(int i=0;i<68;++i)
    {
        for(int j=0;j<no_images*2;++j)
        {
            myfile << landmarks[i][j] << " ";
        }
        myfile << "\n";
    }
    myfile.close();
}

// Traverses the root directory and looks for directories with images in them (currently only .jpg as
// can be seen below, but can be adjusted). All the directories that contain images are put in the 
// paths_with_images vector and will be analyzed with the blink response detection algorithms

void readdirs(string currentpath, std::vector<string>& paths_with_images)
{
    DIR *dir = opendir(currentpath.c_str());
    struct dirent *entry = readdir(dir);

    while (entry != NULL)
    {
        if (entry->d_type == DT_DIR && entry->d_name[0]!='.')
        {
            readdirs(currentpath+"/"+string(entry->d_name), paths_with_images);
        }
        if (string(entry->d_name).find(".jpg") != std::string::npos) 
        {
            paths_with_images.push_back(currentpath);
            closedir(dir);
            return;
        }
        entry = readdir(dir);
    }
    closedir(dir);
}

// The filenames of all images in a directory with a .jpg extension (can be changed) are added to a vector
// The images in this vector will be analyzed by the blink response detection algorithms

void readfiles(std::vector<string>& images_in_dir, string currentpath)
{
    DIR *dir = opendir(currentpath.c_str());
    struct dirent *entry = readdir(dir);

    while (entry != NULL)
    {
        if (string(entry->d_name).find(".jpg") != std::string::npos) 
        {
            images_in_dir.push_back(currentpath+"/"+string(entry->d_name));
        }
        entry = readdir(dir);
    }
    closedir(dir);
    std::sort(images_in_dir.begin(), images_in_dir.end());
}

// This function loads the trained landmark detection model and allocates memory on the GPU for the images
// as well as for intermediate feature extraction and classification steps. 
// The "shape_predictor_68_face_landmarks.dat" is a file which contains the values for the regression trees 
// of the landmark detection algorithm and should be located in the same folder as the executable. 
// NOTE THAT THIS FUNCTION TAKES TIME (1 second ish), but only has to be executed once no matter how many images are analyzed 
// during execution. Therefore, for the final on-line implementation, something could be added to call this function
// before the experiment starts so it doesn't mess up the real-time eyeblink response detection speed. 

void loadTrainedDetectionModels(shape_predictor& sp, frontal_face_detector& detector, std::vector<double>& threshs, 
    int& no_scales, int& width_org_img, int& height_org_img, string initial_image)
{
    // This is where the values of the trained landmark detector (shape predictor) and face detector get loaded.
    deserialize("shape_predictor_68_face_landmarks.dat") >> sp;
    // This function also sends the classification filter values (the weights for the features) to the GPU. The 
    // ''scan_fhog_pyramid.h'' file in the dlib library (image processing directory) is slightly altered in order
    // to achieve this.  
    detector = get_frontal_face_detector();

    dlib::pyramid_down<6> pyr;  

    initialize_gpu_memory(detector, no_scales, pyr, initial_image, width_org_img, height_org_img);

    // These thresholds are from the face detector classifier and used later for the non-maximum suppression
    // step to compute which of the detections has the highest detection-confidence score
    int thresh_index = detector.get_scanner().get_num_dimensions();
    for(int fb_num=0;fb_num<NO_FILTERS;fb_num++)
    {
        double thresh = detector.get_w(fb_num)(thresh_index);
        threshs.push_back(thresh);
    }
}

// Main function which runs for every directory containing images

void extractLandmarkInfo(const std::vector<string>& images_in_dir, const int& no_scales, frontal_face_detector& detector, shape_predictor sp, 
    int& width_org_img, int& height_org_img, string dir_name, std::vector<double>& threshs, const int img_size)
{
    dlib::pyramid_down<6> pyr;  

    // Shared array that is returned from the GPU to the CPU which contains the detections 
    // for ALL images of the batch (depending on the value of NO_THREADS, see version.h file) on 
    // which the face detection algorithm was performed on the GPU.  
    float host_faces[NO_THREADS*(MAX_NO_SCALES*NO_FILTERS*MAX_FACES*5+1)];
    
    std::vector<int> landmarks[NO_THREADS][68];
    unsigned char* image = new unsigned char[width_org_img*height_org_img*NO_THREADS];
    std::vector<rect_detection> dets_accum;
    std::vector<rectangle> dets;
    std::vector<full_object_detection> shapes;

    // Private array of face location(s) of image loaded by CPU thread
    float thread_host_faces[MAX_NO_SCALES*NO_FILTERS*MAX_FACES*5+1]; 

    // This thread_img and previous_img is needed for the pipelined structure. While the landmark
    // detection algorithm is performed on the previous batch of images, the next batch can 
    // already be loaded and sent to the GPU for face detection
    array2d<unsigned char> thread_img[NO_THREADS];
    array2d<unsigned char> previous_img[NO_THREADS];
    int pipeline = 0;

    // Loop over all the images provided in the directory
    int no_images = images_in_dir.size();
    int no_iterations = ceil(float(no_images)/NO_THREADS);
    printf("no_iterations:%i\n",no_iterations);
    
    // For NVVP profiler and timing, not really needed in current version
    // initialize_profiler();
    // struct timeval startt, endt, result;
    // result.tv_sec = 0;
    // result.tv_usec= 0;   
    printf("GO!\n");
    // gettimeofday (&startt, NULL);

    // This is a pipelined version to overlap execution on CPU and GPU. Sequentially, the steps would 
    // globally be as follows. combineThreadImages -> gpuDetectFaces -> gpuGetFaces -> detectLandmarks.
    // ADDITIONALLY, there is also (prototype) version available in this folder which makes use of OMP ''tasks'',
    // which might work better with the final on-line implementation because it can function in a while
    //loop, in contrast to the ''#pragma omp parallel'' for construct.

    for(int i=0;i<no_iterations+2;++i)
    {
        if(pipeline>0 && pipeline<(no_iterations+1))
        {
            gpuDetectFaces(image, no_scales); //Only issuing kernel launches, not waiting for GPU sync
        }
         #pragma omp parallel num_threads(NO_THREADS) private (thread_host_faces, dets, dets_accum, shapes)                  
        {
            int thread_id = omp_get_thread_num();
            if(pipeline>1)
            { 
                detectLandmarks(thread_host_faces, host_faces, thread_id, threshs, dets_accum, dets, detector, pyr, sp, previous_img[thread_id], landmarks[thread_id]);
                #ifdef DEBUG_PRINT
                debugPrint(images_in_dir, dets, previous_img[thread_id], shapes, (thread_id*no_iterations+i-1));
                #endif
            }
            if(pipeline<no_iterations)
            { 
                previous_img[thread_id].swap(thread_img[thread_id]);
                combineThreadImages(thread_img[thread_id], image, images_in_dir, no_iterations, i, no_images, img_size);
            }
        } //implicit barrier
        if(pipeline>0 && pipeline<(no_iterations+1))
        {
            gpuGetFaces(host_faces, no_scales); //gpu should be finished after landmark detection
        } 
        pipeline++;    
    }

    printf("finished!\n");

    // Each CPU thread processed a part of the images in the directory and stores the landmarks for those images
    // in a vector. For example, in case of 16 images and 4 CPU threads, CPU 0 processes image 0,1,2,3, CPU 1 
    // processes 4,5,6,7 etc. When all images are finished, the results get combined on the first CPU thread.

    for(int i=1; i<NO_THREADS; ++i)
    {
        for(int j=0; j<68; ++j)
        {
            landmarks[0][j].insert(landmarks[0][j].end(), landmarks[i][j].begin(), landmarks[i][j].end());
        }
    }

    printf("Writing results\n");
    std::replace( dir_name.begin(), dir_name.end(), '/', '_'); // replace all '/' to '_'
    
    // This is not really robust in the current version, the file name depends on the root directory
    // should be changed so that it only depends on subject ID,  session id, block id and trial id. 
    // Also, the landmark directory is hard coded here, but it could be changed so you can specify 
    // it as an argument and don't have to recompile all the time when you want to change this. 

    dir_name.erase(0,25); // VARIABLE WITH ROOT DIR NAME, DEPENDS ON HOW MUCH OF THE PATH TO THE IMAGES YOU WANT TO ERASE FOR FILENAME
    writeLandmarks(landmarks[0], "/home/paul/Medical/landmarkresults_new/landmarks/"+dir_name+"_landmarkdata.txt",no_images);

    // Also used for profiling and timing
    // gettimeofday (&endt, NULL);
    // result.tv_usec += (endt.tv_sec*1000000+endt.tv_usec) - (startt.tv_sec*1000000+startt.tv_usec);
    // end_profiler();
    // printf("TIME ELAPSED TOTAL: %ld.%06ld | \n", result.tv_usec/1000000, result.tv_usec%1000000);
    // printf("TIME ELAPSED PER IMAGE: %ld.%06ld | \n", (result.tv_usec/1000000)/no_images, (result.tv_usec%100000000)/no_images);
}

// FOR MAXIMUM PERFORMANCE, EXECUTE THE "export OMP_PROC_BIND=true" COMMAND IN THE COMMAND LINE. THIS 
// COULD PROBABLY BE EMBEDDED IN THE CURRENT CODE BUT IS NOT DONE IN THIS VERSION. The command makes sure
// that logical threads keep running on the same physical processor core, which is beneficial because of 
// previously cached data.   

int main(int argc, char** argv)
{
    try
    {
        // Root directory should be specified as argument to the program
        if (argc != 2)
        {
            cout << "Give root directory with all the images as argument to this program" << endl;
            return 0;
        }
        std::vector<string> paths_with_images;
        std::string PATH = string(argv[1]);
        std::string currentpath = PATH;
        std::vector<string> images_in_dir;
        std::vector<double> threshs;

        // Read all the directories in the root directory that contain image files
        readdirs(currentpath, paths_with_images);
        printf("Number of path with images:%li\n",paths_with_images.size());

        // This is done for the loadTrainedDetectionModel step. In order to know how much memory
        // needs to be allocated on the GPU, we need to have an example image. Therefore we read 
        // the filenames of the first directory containing images, and use one of these images later
        // to see how much memory needs to be allocated.
        readfiles(images_in_dir, paths_with_images[0]);

        shape_predictor sp;
        frontal_face_detector detector;
         
        int no_scales;
        int width_org_img;
        int height_org_img;

        loadTrainedDetectionModels(sp, detector, threshs, no_scales, width_org_img, height_org_img, images_in_dir[0]);

        // For every directory in the root directory containing images, perform the complete eyeblink-response detection algorithm
        for(int i=0;i<paths_with_images.size();++i)
        {
            cout<<paths_with_images[i]<<endl;
            images_in_dir.clear();
            readfiles(images_in_dir,paths_with_images[i]);
            extractLandmarkInfo(images_in_dir, no_scales, detector, sp, width_org_img, height_org_img, paths_with_images[i], threshs, width_org_img*height_org_img);
        }
    }
    catch (exception& e)
    {
        cout << "\nexception thrown!" << endl;
        cout << e.what() << endl;
    }
}
// The contents of this file are in the public domain. See LICENSE_FOR_EXAMPLE_PROGRAMS.txt
/*

    This example program shows how to find frontal human faces in an image.  In
    particular, this program shows how you can take a list of images from the
    command line and display each on the screen with red boxes overlaid on each
    human face.

    The examples/faces folder contains some jpg images of people.  You can run
    this program on them and see the detections by executing the following command:
        ./face_detection_ex faces/*.jpg

    
    This face detector is made using the now classic Histogram of Oriented
    Gradients (HOG) feature combined with a linear classifier, an image pyramid,
    and sliding window detection scheme.  This type of object detector is fairly
    general and capable of detecting many types of semi-rigid objects in
    addition to human faces.  Therefore, if you are interested in making your
    own object detectors then read the fhog_object_detector_ex.cpp example
    program.  It shows how to use the machine learning tools which were used to
    create dlib's face detector. 


    Finally, note that the face detector is fastest when compiled with at least
    SSE2 instructions enabled.  So if you are using a PC with an Intel or AMD
    chip then you should enable at least SSE2 instructions.  If you are using
    cmake to compile this program you can enable them by using one of the
    following commands when you create the build project:
        cmake path_to_dlib_root/examples -DUSE_SSE2_INSTRUCTIONS=ON
        cmake path_to_dlib_root/examples -DUSE_SSE4_INSTRUCTIONS=ON
        cmake path_to_dlib_root/examples -DUSE_AVX_INSTRUCTIONS=ON
    This will set the appropriate compiler options for GCC, clang, Visual
    Studio, or the Intel compiler.  If you are using another compiler then you
    need to consult your compiler's manual to determine how to enable these
    instructions.  Note that AVX is the fastest but requires a CPU from at least
    2011.  SSE4 is the next fastest and is supported by most current machines.  
*/

#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <iostream>
#include <sys/time.h>
#include "../version.h"
#include "../dlib/image_processing/box_overlap_testing.h"
#include "../dlib/image_transforms/image_pyramid.h"
#include <omp.h>
#include <math.h>

using namespace dlib;
using namespace std;

extern void initialize_profiler();
extern void end_profiler();
extern void gpuDetectFaces(unsigned char* image, const int no_scales);
extern void gpuGetFaces(float* host_faces, const int no_scales);      
extern void gpu_allocate_memory_constant_image_size(int height_org_img, int width_org_img, int no_scales, int cell_size, int scale_factor, 
    int padding_rows_offset, int padding_cols_offset, int filter_rows_padding, int filter_cols_padding);

// ----------------------------------------------------------------------------------------


// Function to see how much memory should be allocated on the GPU.
// Based on the height and width of the first image of the sequence
// we compute how many times the image needs to be downscaled in a 
// pyramid-like fashion in order to detect faces from all sizes. 
// Based on this number of scales, a certain amount of memory is 
// allocated on the GPU for all scales of the images for feature 
// extraction and classification steps

void initialize_gpu_memory(frontal_face_detector detector, int& no_scales, dlib::pyramid_down<6>& pyr, char** argv, int& width_org_img, int& height_org_img)
{
    array2d<unsigned char> initial_image;
    load_image(initial_image, argv[1]);


    // "pyramid_up" can be used to upscale the image to detect smaller images. 
    // However, in the current eyeblink-conditioning setup this is not necessary
    // The biggest scale is even skipped because it searches for relatively small
    // faces in the image, out of the requirements for this project (>20% of the image)

    // pyramid_up(initial_image);

    width_org_img = initial_image.nc();
    height_org_img = initial_image.nr();

    const unsigned long min_pyramid_layer_width = detector.get_scanner().get_min_pyramid_layer_width();
    const unsigned long min_pyramid_layer_height = detector.get_scanner().get_min_pyramid_layer_height();
    const unsigned long max_pyramid_levels = detector.get_scanner().get_max_pyramid_levels();
    const unsigned long cell_size = detector.get_scanner().get_cell_size();
    const unsigned long width = detector.get_scanner().get_fhog_window_width();
    const unsigned long height = detector.get_scanner().get_fhog_window_height();
    const int padding_rows_offset = (height-1)/2;
    const int padding_cols_offset = (width-1)/2;
    no_scales = 0;  

    rectangle rect_img;
    rect_img = resize_rect(rect_img, width_org_img, height_org_img);

    double scale_factor = pyramid_rate(pyr);        //0.83333
    int N = int((scale_factor)/(1-scale_factor)+1); //6 
    do
    {
        rect_img = pyr.rect_down(rect_img);
        ++no_scales;
    } while (rect_img.width() >= min_pyramid_layer_width && rect_img.height() >= min_pyramid_layer_height &&
        no_scales < max_pyramid_levels);

    gpu_allocate_memory_constant_image_size(height_org_img, width_org_img, no_scales, cell_size, N, padding_rows_offset, padding_cols_offset, height, width);
}

// Helper function for non-maximum suppression to see if any of the detections 
// are of the same object. 

bool overlaps_any_box(const std::vector<rectangle>& rects, const dlib::rectangle& rect, test_box_overlap boxes_overlap)
{
    for (unsigned long i = 0; i < rects.size(); ++i)
    {
        if (boxes_overlap(rects[i], rect))
            return true;
    }
    return false;   
}


// Each CPU thread 



void convert_to_rects(float* host_faces, std::vector<double>& threshs, std::vector<rect_detection> dets_accum, 
    std::vector<rectangle>& dets, frontal_face_detector detector, dlib::pyramid_down<6>& pyr)
{
    int no_faces = (int)host_faces[MAX_NO_SCALES*NO_FILTERS*MAX_FACES*5];

    for(int i=0;i<no_faces;++i)
    {
        int x = int(host_faces[5*i]);
        int y = int(host_faces[5*i+1]);
        float score = host_faces[5*i+2];
        int scale = int(host_faces[5*i+3]);
        int fb = int(host_faces[5*i+4]);
        rectangle rect = detector.get_scanner().get_feature_extractor().feats_to_image(dlib::centered_rect(dlib::point(x,y),
          detector.get_scanner().get_fhog_window_width()-2*detector.get_scanner().get_padding(), detector.get_scanner().get_fhog_window_height()-2*detector.get_scanner().get_padding()),
        detector.get_scanner().get_cell_size(), detector.get_scanner().get_fhog_window_height(), detector.get_scanner().get_fhog_window_width());
        rect = pyr.rect_up(rect, scale); 
        rect_detection temp;
        temp.detection_confidence = score-threshs[fb];
        temp.weight_index = fb;
        temp.rect = rect; 
        dets_accum.push_back(temp);   
    }
    dets.clear();
    std::sort(dets_accum.rbegin(), dets_accum.rend());
    for (unsigned long i = 0; i < dets_accum.size(); ++i)
    {
        if (overlaps_any_box(dets, dets_accum[i].rect, detector.get_overlap_tester()))
            continue;

        dets.push_back(dets_accum[i].rect);
    }

    std::sort(dets.begin(), dets.end(),[](rectangle a, rectangle b) {
        return b.area() < a.area();   
    });
    dets_accum.clear();
}

float euclideanDistance(dlib::point eye_one, dlib::point eye_two)
{
    return sqrt((pow((eye_one.x()-eye_two.x()),2))+(pow((eye_one.y()-eye_two.y()),2)));
}

void computeEARValue(dlib::full_object_detection& shape, std::vector<float>& ear_values)
{
    float ear_value;
    ear_value = (euclideanDistance(shape.part(43),shape.part(47)) + euclideanDistance(shape.part(44),shape.part(46)))/
    (2*euclideanDistance(shape.part(42),shape.part(45)));
    ear_values.push_back(ear_value);
}   

void plotIMG(std::vector<float>& ear_values)
{
    FILE *gnuplot = popen("gnuplot", "w");
    fprintf(gnuplot, "plot '-'\n");
    for (int i = 0; i < ear_values.size(); i++)
    {
        if(ear_values[i]!=0.0)
        {
            fprintf(gnuplot, "%i %f\n", i, ear_values[i]);
        }
    }
    fprintf(gnuplot, "e\n");
    fflush(gnuplot);
    std::cin.get();
}

void combineThreadImages(array2d<unsigned char>& thread_img, unsigned char* image, char** argv, const int& no_iterations, const int& i, const int& no_images, const int img_size)
{
    int thread_id = omp_get_thread_num();
    unsigned char* thread_image_array = new unsigned char[img_size];

    //if no_images is not divisable by no_threads
    if(thread_id*no_iterations+i < no_images)
    {
        load_image(thread_img, argv[1+thread_id*no_iterations+i]);
        for(int r = 0; r<thread_img.nr(); r++)
        {
            for(int c = 0; c<thread_img.nc(); c++)
            {
                thread_image_array[r*thread_img.nc()+c] = dlib::get_pixel_intensity(thread_img[r][c]);
            }
        }
        memcpy(image+thread_id*thread_img.nc()*thread_img.nr(),thread_image_array,img_size*sizeof(unsigned char));
    }
    else
    {  
        for(int r = 0; r<thread_img.nr(); r++)
        {
            for(int c = 0; c<thread_img.nc(); c++)
            {
                thread_image_array[r*thread_img.nc()+c] = 0;

            }
        }
        memcpy(image+thread_id*thread_img.nc()*thread_img.nr(),thread_image_array,img_size*sizeof(unsigned char));
    }
}

void print_proc_affinity()
{
    printf( "Hello world from thread %d of %d running on cpu %2d!\n", 
        omp_get_thread_num(), 
        omp_get_num_threads(),
        sched_getcpu());
}

void debugPrint(char** argv, std::vector<dlib::rectangle>& dets, array2d<unsigned char>& previous_img, std::vector<full_object_detection>& shapes, int img_number)
{
    cout << "processing image " << argv[img_number] << endl;
    cout << "Number of faces detected: " << dets.size() << endl;
    image_window win;

    win.clear_overlay();
    win.set_image(previous_img);
    win.add_overlay(render_face_detections(shapes));   
    win.add_overlay(dets, rgb_pixel(255,0,0)); 
    cout << "Hit enter to process the next image..." << endl;
    cin.get();
}

void detectLandmarks(float* thread_host_faces, float* host_faces, int thread_id, std::vector<double>& threshs, std::vector<rect_detection>& dets_accum, 
    std::vector<rectangle>& dets, frontal_face_detector& detector, pyramid_down<6>& pyr, shape_predictor& sp, array2d<unsigned char>& previous_img, std::vector<float>& ear_values, std::vector<full_object_detection>& shapes)
{
    memcpy(thread_host_faces, host_faces+thread_id*(MAX_NO_SCALES*NO_FILTERS*MAX_FACES*5+1), (MAX_NO_SCALES*NO_FILTERS*MAX_FACES*5+1)*sizeof(float));
    convert_to_rects(thread_host_faces, threshs, dets_accum, dets, detector, pyr);
    if(!dets.empty())
    {
        dlib::full_object_detection shape = sp(previous_img, dets[0]);
        shapes.push_back(shape);
        computeEARValue(shape, ear_values);     
    }
    else
    {
        printf("No faces, or more than 1 face, detected\n");
        ear_values.push_back(0);
    }
}

int main(int argc, char** argv)
{  
    try
    {
        if (argc == 1)
        {
            cout << "Give some image files as arguments to this program." << endl;
            return 0;
        }

        shape_predictor sp;
        deserialize("shape_predictor_68_face_landmarks.dat") >> sp;

        dlib::pyramid_down<6> pyr;  

        frontal_face_detector detector = get_frontal_face_detector();
        
        int no_scales;
        int width_org_img;
        int height_org_img;
        initialize_gpu_memory(detector, no_scales, pyr, argv, width_org_img, height_org_img);

        std::vector<double> threshs;
        int thresh_index = detector.get_scanner().get_num_dimensions();
        for(int fb_num=0;fb_num<NO_FILTERS;fb_num++)
        {
            double thresh = detector.get_w(fb_num)(thresh_index);
            threshs.push_back(thresh);
        }

        struct timeval startt, endt, result;
        result.tv_sec = 0;
        result.tv_usec= 0; 

        float host_faces[NO_THREADS*(MAX_NO_SCALES*NO_FILTERS*MAX_FACES*5+1)];
        std::vector<float> ear_values[NO_THREADS];
        unsigned char* image = new unsigned char[width_org_img*height_org_img*NO_THREADS];

        initialize_profiler();  

        std::vector<rect_detection> dets_accum;
        std::vector<rectangle> dets;
        std::vector<full_object_detection> shapes;
        float thread_host_faces[MAX_NO_SCALES*NO_FILTERS*MAX_FACES*5+1]; 
        array2d<unsigned char> thread_img[NO_THREADS];
        array2d<unsigned char> previous_img[NO_THREADS];
        int pipeline = 0;

        // Loop over all the images provided on the command line.
        int no_images = argc-1;
        int no_iterations = ceil(float(no_images)/NO_THREADS);
        printf("no_iterations:%i\n",no_iterations);

        printf("GO!\n");
        gettimeofday (&startt, NULL);

        for(int i=0;i<no_iterations+2;++i)
        {
            if(pipeline>0 && pipeline<(no_iterations+1))
            {
                gpuDetectFaces(image, no_scales); //Only issueing kernel launches, not waiting for GPU sync
            }
            #pragma omp parallel num_threads(NO_THREADS) private (thread_host_faces, dets, dets_accum, shapes)                  
            {
                int thread_id = omp_get_thread_num();
                if(pipeline>1)
                { 
                    detectLandmarks(thread_host_faces, host_faces, thread_id, threshs, dets_accum, dets, detector, pyr, sp, previous_img[thread_id], ear_values[thread_id], shapes);
                    #ifdef DEBUG_PRINT
                    debugPrint(argv, dets, previous_img[thread_id], shapes, (1+thread_id*no_iterations+i-1));
                    #endif
                }
                if(pipeline<no_iterations)
                { 
                    previous_img[thread_id].swap(thread_img[thread_id]);
                    combineThreadImages(thread_img[thread_id], image, argv, no_iterations, i, no_images, width_org_img*height_org_img);
                }
            }
            if(pipeline>0 && pipeline<(no_iterations+1))
            {
                gpuGetFaces(host_faces, no_scales); //gpu should be finished after 
            } //implicit barrier
            pipeline++;    
        }

        gettimeofday (&endt, NULL);
        result.tv_usec += (endt.tv_sec*1000000+endt.tv_usec) - (startt.tv_sec*1000000+startt.tv_usec);
        end_profiler();

        for(int i=1; i<NO_THREADS; ++i)
        {
            ear_values[0].insert(ear_values[0].end(), ear_values[i].begin(), ear_values[i].end());
        }
        plotIMG(ear_values[0]);

        // printf("TIME ELAPSED TOTAL: %ld.%06ld | \n", result.tv_usec/1000000, result.tv_usec%1000000);
        // printf("TIME ELAPSED PER IMAGE: %ld.%06ld | \n", (result.tv_usec/1000000)/(argc-1), (result.tv_usec%100000000)/(argc-1));
    }
    catch (exception& e)
    {
        cout << "\nexception thrown!" << endl;
        cout << e.what() << endl;
    }
}